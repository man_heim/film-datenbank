-----1. Bsp Zitate von Person mit Nachname Jackson------

select p.vorname , p.nachname , z.inhalt Zitat
from Person p, Zitat z
where p.sozialVersicherungsNummer = z.sozialVersicherungsNummer and p.sozialVersicherungsNummer in (select Person.sozialVersicherungsNummer from Person where nachname = 'Jackson');


-----2. Bsp Rollenamen von Person mit Nachname Jackson-----

select concat( p.Vorname,p.nachname) Name, f.rollenname
from Person p, Filmrolle f, PersonFilm x
where x.sozialVersicherungsNummer = p.sozialVersicherungsNummer and x.filmRollenID = f.filmrollenID  and f.filmrollenID in (select PersonFilm.filmrollenID
                       from PersonFilm
                       where sozialVersicherungsNummer in
                             (select Person.sozialVersicherungsNummer from Person where p.nachname = 'Jackson'));

-----3. Bsp Filme von Person mit Nachname Egerton---------------------------

select concat(p.vorname, p.nachname) Name, f.titel
from Person p, Film f, PersonFilm x
where x.sozialVersicherungsNummer=p.sozialVersicherungsNummer and x.filmNr= f.filmNr and f.filmNr in (select PersonFilm.filmNr
                 from PersonFilm
                 where sozialVersicherungsNummer in
                       (select Person.sozialVersicherungsNummer from Person where p.nachname = 'Egerton'));

--------4.Bsp Filme + zugehörige Ticket-Umsätze----------------

select f.titel, u.ticketUmsatz
from Film f
         left outer join Umsatz u on f.verkaufsID = u.verkaufsID;

---------5.Bsp Filme + zugehörige Umsätze

select f.titel, u.ticketUmsatz + u.blu_rayUmsatz + u.dvdUmsatz + u.lizenzUmsatz Gesamtumsatz
from Film f
         left outer join Umsatz u on f.verkaufsID = u.verkaufsID;
