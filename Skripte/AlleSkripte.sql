﻿-----Constraints Löschen, dann Tabellen und Sequences----------------------

alter table Zugang
    drop constraint Zugang_pk;
alter table Zitat
    drop constraint Zitat_fk2;
alter table Zitat
    drop constraint Zitat_fk1;
alter table PersonFilm
    drop constraint PersonFilm_fk3;
alter table PersonFilm
    drop constraint PersonFilm_fk2;
alter table PersonFilm
    drop constraint PersonFilm_fk1;
alter table Film
    drop constraint Film_fk2;
alter table Film
    drop constraint Film_fk1;
alter table Zitat
    drop constraint Zitat_pk;
alter table PersonFilm
    drop constraint PersonFilm_pk;
alter table Film
    drop constraint Film_pk;
alter table Produktionsfirma
    drop constraint Produktionsfimra_pk;
alter table Produktionsfirma
    drop constraint Produktionsfimra_u1;
alter table Umsatz
    drop constraint Verkaufszahlen_pk;
alter table Filmrolle
    drop constraint Filmrolle_pk;

drop table Zugang;
drop table Zitat;
drop table PersonFilm;
drop table Person;
drop table Film;
drop table Produktionsfirma;
drop table Umsatz;
drop table Filmrolle;

drop sequence Zitat_count;
drop sequence PersonFilm_count;
drop sequence person_count;
drop sequence filmNr_count;
drop sequence firma_count;
drop sequence umsatz_count;
drop sequence filmrollen_count;

------Erst Tabellen erstellen, danach die Bedingungen------------------------------------------
------Besser für restartfähgikeit--------------------------------------------------

create table Zugang
(
    benutzername char(20),
    rolle        char(20) default('user')  not null ,
    passwort     varchar(40) not null
);

create table Filmrolle
(
    rollenname   varchar(40) null,
    filmrollenID integer
);
create table Umsatz
(
    ticketUmsatz  float default (0),
    blu_rayUmsatz float default (0),
    dvdUmsatz     float default (0),
    lizenzUmsatz  float default (0),
    verkaufsID    integer
);

create table Produktionsfirma
(
    firmenName varchar(20) not null,
    adresse    varchar(60) not null,
    firmenID   integer
);


create table Person
(
    sozialVersicherungsNummer integer,
    vorname                   varchar(30) not null,
    nachname                  varchar(30) not null,
    "alter"                   integer     not null,
    geburtstage               date        not null,
    beruf                     varchar(30) null
);


create table Film
(
    filmNr           integer,
    titel            varchar(40)  not null,
    erscheinungsjahr char(4)      not null,
    genre            varchar(40)  null,
    plotOutline      varchar(500) null,
    laenge           integer      not null,
    verkaufsID       integer      not null,
    firmenID         integer      not null
);

create table PersonFilm
(
    personFilmID              integer,
    sozialVersicherungsNummer integer not null,
    filmNr                    integer not null,
    filmRollenID              integer null
);

create table Zitat
(
    zitatID                   integer,
    inhalt                    varchar(400) not null,
    filmNr                    integer      not null,
    sozialVersicherungsNummer integer      not null
);


alter table Zugang
    add constraint Zugang_pk primary key (benutzername);
alter table Filmrolle
    ADD CONSTRAINT Filmrolle_pk primary key (filmrollenID);
CREATE SEQUENCE filmrollen_count START WITH 4000;
CREATE OR REPLACE TRIGGER start_count_filmrolle
    BEFORE INSERT
    ON Filmrolle
    FOR EACH ROW
BEGIN
    SELECT filmrollen_count.NEXTVAL
    INTO :new.filmrollenID
    FROM dual;
END;

alter table Umsatz
    ADD CONSTRAINT Verkaufszahlen_pk primary key (verkaufsID);
CREATE SEQUENCE umsatz_count START WITH 2000;
CREATE OR REPLACE TRIGGER start_count_umsatz
    BEFORE INSERT
    ON Umsatz
    FOR EACH ROW
BEGIN
    SELECT umsatz_count.NEXTVAL
    INTO :new.verkaufsID
    FROM dual;
END;
alter table Produktionsfirma
    ADD CONSTRAINT Produktionsfimra_u1 unique (firmenName);
alter table Produktionsfirma
    ADD CONSTRAINT Produktionsfimra_u2 unique (adresse);
alter table Produktionsfirma
    ADD CONSTRAINT Produktionsfimra_pk primary key (firmenID);
CREATE SEQUENCE firma_count START WITH 1000;
CREATE OR REPLACE TRIGGER start_count_firma
    BEFORE INSERT
    ON Produktionsfirma
    FOR EACH ROW
BEGIN
    SELECT firma_count.NEXTVAL
    INTO :new.firmenID
    FROM dual;
END;
alter table Film
    ADD CONSTRAINT Film_pk primary key (filmNr);
alter table Film
    ADD CONSTRAINT Film_fk1 foreign key (verkaufsID) references Umsatz (verkaufsID);
alter table Film
    ADD CONSTRAINT Film_fk2 foreign key (firmenID) references Produktionsfirma (firmenID);
CREATE SEQUENCE filmNr_count START WITH 3000;
CREATE OR REPLACE TRIGGER start_count_filmNr
    BEFORE INSERT
    ON Film
    FOR EACH ROW
BEGIN
    SELECT filmNr_count.NEXTVAL
    INTO :new.filmNr
    FROM dual;
END;
alter table Person
    ADD CONSTRAINT Person_pk primary key (sozialVersicherungsNummer);
CREATE SEQUENCE person_count START WITH 5000;
CREATE OR REPLACE TRIGGER start_person_count
    BEFORE INSERT
    ON Person
    FOR EACH ROW
BEGIN
    SELECT person_count.NEXTVAL
    INTO :new.sozialVersicherungsNummer
    FROM dual;
END;
alter table PersonFilm
    ADD CONSTRAINT PersonFilm_pk primary key (personFilmID);
alter table PersonFilm
    ADD CONSTRAINT PersonFilm_fk1 foreign key (sozialVersicherungsNummer) references Person (sozialVersicherungsNummer);
alter table PersonFilm
    ADD CONSTRAINT PersonFilm_fk2 foreign key (filmNr) references Film (filmNr);
alter table PersonFilm
    ADD CONSTRAINT PersonFilm_fk3 foreign key (filmRollenID) references Filmrolle (filmrollenID);
CREATE SEQUENCE PersonFilm_count START WITH 6000;
CREATE OR REPLACE TRIGGER start_personFilm_count
    BEFORE INSERT
    ON PersonFilm
    FOR EACH ROW
BEGIN
    SELECT personFilm_count.NEXTVAL
    INTO :new.personFilmID
    FROM dual;
END;
alter table Zitat
    ADD CONSTRAINT Zitat_pk primary key (zitatID);
alter table Zitat
    ADD CONSTRAINT Zitat_fk1 foreign key (filmNr) references Film (filmNr);
alter table Zitat
    ADD CONSTRAINT Zitat_fk2 foreign key (sozialVersicherungsNummer) references Person (sozialVersicherungsNummer);
CREATE SEQUENCE Zitat_count START WITH 7000;
CREATE OR REPLACE TRIGGER start_Zitat_count
    BEFORE INSERT
    ON Zitat
    FOR EACH ROW
BEGIN
    SELECT Zitat_count.NEXTVAL
    INTO :new.zitatID
    FROM dual;
END;

-----------------------------------------INSERT-SKRIPTE-----------------------------------------------

insert into Zugang(benutzername, rolle, passwort) VALUES ('man.heim','Administrator','1234');
commit;
insert into Zugang(benutzername, rolle, passwort) VALUES ('walt_disney123','Kinobetreiber','passwort');
commit;
insert into Zugang(benutzername,  passwort) VALUES ('filmFan500','lol');
commit;

--------1. Film mit zwei Personen (1 Schauspieler + 1 Regisseur/Schauspieler)-------------------------------------------------------------

insert into Produktionsfirma (Produktionsfirma.firmenName, Produktionsfirma.adresse)
values ('Miramax', 'Ahmed Bin Ali St, Doha, Katar');
commit;

insert into Umsatz (ticketUmsatz, blu_rayUmsatz, dvdUmsatz, lizenzUmsatz)
VALUES (213.45, 1.45, 9.23, 12.65);
commit;

insert into Film (titel, erscheinungsjahr, genre, plotOutline, laenge, verkaufsID, firmenID)
values ('Pulp Ficiton', 1994, 'Gangsterfilm, Thriller, Kriminalfilm',
        'Pulp Fiction: Quentin Tarantinos stilistisch und formal brillante Gangsterfilm-Anthologie -der beste und innovativste Film der Neunziger!" (https://www.kino.de/film/pulp-fiction-1994/ (Stand: 14.06.19)',
        154, 2000, 1000);
commit;


insert into Filmrolle (Filmrolle.rollenname)
values ('Jules Winnifield');
commit;


insert into Person (Person.vorname, Person.nachname, Person."alter",
                    Person.geburtstage, Person.beruf)
values ('Quentin', 'Tarantino', floor((current_date - to_date('29.03.1963', 'DD.MM.YYYY')) / 365),
        to_date('29.03.1963', 'DD.MM.YYYY'), 'Regisseur, Schauspieler');
commit;


insert into Person (Person.vorname, Person.nachname, Person."alter",
                    Person.geburtstage, Person.beruf)
values ('Samuel Leroy', 'Jackson', floor((current_date - to_date('21.12.1948', 'DD.MM.YYYY')) / 365),
        to_date('21.12.1948', 'DD.MM.YYYY'), 'Schauspieler');
commit;


insert into PersonFilm (PersonFilm.sozialVersicherungsNummer, PersonFilm.filmNr)
values (5000, 3000);
commit;


insert into PersonFilm (PersonFilm.sozialVersicherungsNummer, PersonFilm.filmNr,
                        PersonFilm.filmRollenID)
values (5001, 3000, 4000);
commit;


insert into Zitat(inhalt, filmNr, sozialVersicherungsNummer)
VALUES ('The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men.' ||
        ' Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of the darkness, for he is truly his brother''s ' ||
        'keeper and the finder of lost children', 3000, 5001);
commit;


--------------2.Film (Gleicher Schauspieler + Neuer Schauspieler + Regisseur) ----------------------------------------------------------------------------------------------


insert into Produktionsfirma(Produktionsfirma.firmenName, Produktionsfirma.adresse)
values ('20th Century Fox', '2121 Avenue of the Stars, Los Angeles, CA 90067, USA');
commit;

insert into Umsatz(ticketUmsatz, blu_rayUmsatz, dvdUmsatz, lizenzUmsatz)
VALUES (414.35, 2.36, 0.98, 1.23);
commit;

insert into Film(Film.titel, Film.erscheinungsjahr, Film.genre, Film.plotOutline, Film.laenge,
                 Film.verkaufsID, Film.firmenID)
values ('Kingsman: The Secret Service', 2014, 'Action, Komödie',
        '"Kingsman: The Secret Service: Verfilmung der Comic-Reihe um eine britische Agenten-Spezialeinheit mit Colin Firth als stylishem Haudrauf. Vom "Kick-Ass"-Team!" (https://www.kino.de/film/kingsman-the-secret-service-2014/ (Stand: 14.06.19)',
        129, 2001, 1001);
commit;


insert into Filmrolle(Filmrolle.rollenname)
values ('Valentine');
commit;


insert into Filmrolle(Filmrolle.rollenname)
values ('Gary „Eggsy“ Unwin');
commit;


insert into Person (Person.vorname, Person.nachname, Person."alter",
                    Person.geburtstage, Person.beruf)
values ('Taron', 'Egerton', floor((current_date - to_date('10.11.1989', 'DD.MM.YYYY')) / 365),
        to_date('10.11.1989', 'DD.MM.YYYY'), 'Schauspieler');
commit;


insert into Person (Person.vorname, Person.nachname, Person."alter",
                    Person.geburtstage, Person.beruf)
values ('Matthew', 'Vaughn', floor((current_date - to_date('07.03.1971', 'DD.MM.YYYY')) / 365),
        to_date('07.03.1971', 'DD.MM.YYYY'), 'Regisseur');
commit;


insert into PersonFilm(PersonFilm.sozialVersicherungsNummer, PersonFilm.filmNr,
                       PersonFilm.filmRollenID)
values (5001, 3001, 4001);
commit;


insert into PersonFilm(PersonFilm.sozialVersicherungsNummer, PersonFilm.filmNr,
                       PersonFilm.filmRollenID)
values (5002, 3001, 4002);
commit;


insert into PersonFilm (PersonFilm.sozialVersicherungsNummer, PersonFilm.filmNr)
values (5003, 3001);
commit;


insert into Zitat(Zitat.inhalt, Zitat.filmNr, Zitat.sozialVersicherungsNummer)
values ('He made my kill professor Arnold I god damn loved Professor Arnold', 3001, 5001);
commit;


insert into Zitat(Zitat.inhalt, Zitat.filmNr, Zitat.sozialVersicherungsNummer)
values ('You shot a dog just to get a fucking job?', 3001, 5002);
commit;


--------------3.Film (Regisseur + Schauspieler)-----------------------------------------------

insert into Produktionsfirma (Produktionsfirma.firmenName, Produktionsfirma.adresse)
values ('Disney', 'Burbank, Kalifornien, USA');
commit;

insert into Umsatz (ticketUmsatz, blu_rayUmsatz, dvdUmsatz, lizenzUmsatz)
VALUES (798.45, 45.45, 9.67, 2.65);
commit;


insert into Film (titel, erscheinungsjahr, genre, plotOutline, laenge, verkaufsID, firmenID)
values ('Zoomania', 2016, 'Animationsfilm, Komödie',
        'Judy Hopps - frisch vom Lande und der Polizeischule in die große Stadt kommt, muss sie in einem Team knallharter, ziemlich imposanter Cops erst einmal beweisen, dass sie wirklich was drauf hat',
        104, 2002, 1002);
commit;


insert into Filmrolle (Filmrolle.rollenname)
values ('Judy Hopps');
commit;

insert into Person (Person.vorname, Person.nachname, Person."alter",
                    Person.geburtstage, Person.beruf)
values ('Ginnifer', 'Goodwin', floor((current_date - to_date('22.05.1978', 'DD.MM.YYYY')) / 365),
        to_date('22.05.1978', 'DD.MM.YYYY'), 'Schauspieler');
commit;


insert into Person (Person.vorname, Person.nachname, Person."alter",
                    Person.geburtstage, Person.beruf)
values ('Byron', 'Howard', floor((current_date - to_date('15.07.1968', 'DD.MM.YYYY')) / 365),
        to_date('15.07.1968', 'DD.MM.YYYY'), 'Regisseur');
commit;


insert into PersonFilm (PersonFilm.sozialVersicherungsNummer, PersonFilm.filmNr,
                        PersonFilm.filmRollenID)
values (5004, 3002, 4003);
commit;



insert into PersonFilm (PersonFilm.sozialVersicherungsNummer, PersonFilm.filmNr)
values (5005, 3002);
commit;


insert into Zitat(inhalt, filmNr, sozialVersicherungsNummer)
VALUES ('Wie sich herausstellte ist das wahre Leben ein bischen komplizierter als ein Spruch auf einem Autoaufkleber. Das wahre Leben... ist chaotisch.',
        3002, 5004);
commit;

--------------------------------------SELECTS------------------------------------------------------------
-----1. Bsp Zitate von Person mit Nachname Jackson------

select p.vorname , p.nachname , z.inhalt Zitat
from Person p, Zitat z
where p.sozialVersicherungsNummer = z.sozialVersicherungsNummer and p.sozialVersicherungsNummer in (select Person.sozialVersicherungsNummer from Person where nachname = 'Jackson');


-----2. Bsp Rollenamen von Person mit Nachname Jackson-----

select concat( p.Vorname,p.nachname) Name, f.rollenname
from Person p, Filmrolle f, PersonFilm x
where x.sozialVersicherungsNummer = p.sozialVersicherungsNummer and x.filmRollenID = f.filmrollenID  and f.filmrollenID in (select PersonFilm.filmrollenID
                       from PersonFilm
                       where sozialVersicherungsNummer in
                             (select Person.sozialVersicherungsNummer from Person where p.nachname = 'Jackson'));

-----3. Bsp Filme von Person mit Nachname Egerton---------------------------

select concat(p.vorname, p.nachname) Name, f.titel
from Person p, Film f, PersonFilm x
where x.sozialVersicherungsNummer=p.sozialVersicherungsNummer and x.filmNr= f.filmNr and f.filmNr in (select PersonFilm.filmNr
                 from PersonFilm
                 where sozialVersicherungsNummer in
                       (select Person.sozialVersicherungsNummer from Person where p.nachname = 'Egerton'));

--------4.Bsp Filme + zugehörige Ticket-Umsätze----------------

select f.titel, u.ticketUmsatz
from Film f
         left outer join Umsatz u on f.verkaufsID = u.verkaufsID;

---------5.Bsp Filme + zugehörige Umsätze

select f.titel, u.ticketUmsatz + u.blu_rayUmsatz + u.dvdUmsatz + u.lizenzUmsatz Gesamtumsatz
from Film f
         left outer join Umsatz u on f.verkaufsID = u.verkaufsID;

-------------------------------------UPDATES----------------------------------------------------------
UPDATE FILM
SET ERSCHEINUNGSJAHR = 1995
WHERE FILMNR = 3000;
COMMIT;

UPDATE FILM
SET LAENGE = 160
WHERE FILMNR = 3001;
COMMIT;

UPDATE PERSON
SET VORNAME = 'Thomas'
WHERE SOZIALVERSICHERUNGSNUMMER = 5002;
COMMIT;

UPDATE UMSATZ
SET DVDUMSATZ = 0.77
WHERE VERKAUFSID = 2001;
COMMIT;

UPDATE PRODUKTIONSFIRMA
SET FIRMENNAME = 'Manamax'
WHERE FIRMENID = 1000;
COMMIT;

update PERSON set nachname='Müller' where sozialVersicherungsNummer in (select sozialVersicherungsNummer from PersonFilm where filmNr in (select filmNr from Film where erscheinungsjahr<2000));
commit;